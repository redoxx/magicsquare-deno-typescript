import { EOL } from "https://deno.land/std/fs/eol.ts";

/**
 * Prints the license of this library
 */
function license(): string {
	return `MagicSquare v1.0.2 (20240102)
MagicSquare Copyright(C) 2014 - 2023 Anoop Manakkalath
<anoop.manakkalath@gmail.com>

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License version 3 as
published by the Free Software Foundation.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.`;
}

/**
 * This generates the odd magic square
 * @return number[][] magic square
 */
function createOddMagicSquare(order: number): number[][] {
	let magicSquare = initializeMagicSquare(order);
	let square = order * order;
	let row = 0;
	let col = Math.trunc(order / 2);
	magicSquare[row][col] = 1;
	for (let num = 1; num < square; num++) {
		row--;
		col++;
		if (row < 0 && col >= order) {
			row += 2;
			col--;
		}
		if (row < 0) {
			row = order - 1;
		}
		if (col >= order) {
			col = 0;
		}
		if (magicSquare[row][col] !== 0) {
			row += 2;
			col--;
		}
		if (magicSquare[row][col] === 0) {
			magicSquare[row][col] = num + 1;
		}
	}
	return magicSquare;
}

/**
 * This generates the doubly even magic square
 * @return number[][] magic square
 */
function createDoublyEvenMagicSquare(order: number): number[][] {
	let magicSquare = initializeMagicSquare(order);
	for (let i = 0; i < order; i++) {
		if (i % 4 === 0 || i % 4 === 3) {
			for (let j = 0; j < order; j++) {
				if (j % 4 === 0 || j % 4 === 3) {
					magicSquare[i][j] = i * order + j + 1;
				} else {
					magicSquare[i][j] = (order - i) * order - j;
				}
			}
		} else {
			for (let j = 0; j < order; j++) {
				if (j % 4 === 1 || j % 4 === 2) {
					magicSquare[i][j] = i * order + j + 1;
				} else {
					magicSquare[i][j] = (order - i) * order - j;
				}
			}
		}
	}
	return magicSquare;
}

/**
 * This generates the singly even magic square
 * @return number[][] magic square
 */
function createSinglyEvenMagicSquare(order: number): number[][] {
	let magicSquare = initializeMagicSquare(order);
	let fourthsQuotient = Math.trunc((order - 2) / 4);
	let half = order / 2;
	let quarter = Math.trunc(half / 2);
	let quarterSquare = half * half;
	let num = 1;
	let row = 0;
	let col = quarter;
	let stored = 0;
	let tmpRow = 0;
	magicSquare[row][col] = num;
	while (num !== quarterSquare) {
		num++;
		row--;
		col++;
		if (row < 0 && col >= half) {
			row += 2;
			col--;
		}
		if (row < 0) {
			row = half - 1;
		}
		if (col >= half) {
			col = 0;
		}
		if (magicSquare[row][col] != 0) {
			row += 2;
			col--;
		}
		if (magicSquare[row][col] === 0) {
			magicSquare[row][col] = num;
		}
	}
	num = quarterSquare + 1;
	row = half;
	col = half + quarter;
	magicSquare[row][col] = num;
	while (num !== quarterSquare * 2) {
		num = num + 1;
		row--;
		col++;
		if (row < half && col >= half * 2) {
			row += 2;
			col--;
		}
		if (row < half) {
			row = 2 * half - 1;
		}
		if (col >= half * 2) {
			col = half;
		}
		if (magicSquare[row][col] !== 0) {
			row += 2;
			col--;
		}
		if (magicSquare[row][col] === 0) {
			magicSquare[row][col] = num;
		}
	}
	num = quarterSquare * 2 + 1;
	row = 0;
	col = half + quarter;
	magicSquare[row][col] = num;
	while (num !== quarterSquare * 3) {
		num = num + 1;
		row--;
		col++;
		if (row < 0 && col >= half * 2) {
			row += 2;
			col--;
		}
		if (row < 0) {
			row = half - 1;
		}
		if (col >= half * 2) {
			col = half;
		}
		if (magicSquare[row][col] !== 0) {
			row += 2;
			col--;
		}
		if (magicSquare[row][col] === 0) {
			magicSquare[row][col] = num;
		}
	}
	num = quarterSquare * 3 + 1;
	row = half;
	col = quarter;
	magicSquare[row][col] = num;
	while (num != quarterSquare * 4) {
		num = num + 1;
		row--;
		col++;
		if (row < half && col >= half) {
			row += 2;
			col--;
		}
		if (row < half) {
			row = 2 * half - 1;
		}
		if (col >= half) {
			col = 0;
		}
		if (magicSquare[row][col] !== 0) {
			row += 2;
			col--;
		}
		if (magicSquare[row][col] === 0) {
			magicSquare[row][col] = num;
		}
	}
	for (let i = 0; i < fourthsQuotient; i++) {
		tmpRow = 0;
		while (tmpRow < half) {
			if (i === 0 && tmpRow === fourthsQuotient) {
				tmpRow++;
				continue;
			}
			stored = magicSquare[tmpRow][i];
			magicSquare[tmpRow][i] = magicSquare[tmpRow + half][i];
			magicSquare[tmpRow + half][i] = stored;
			tmpRow++;
		}
	}
	stored = magicSquare[fourthsQuotient][fourthsQuotient];
	magicSquare[fourthsQuotient][fourthsQuotient] = magicSquare[fourthsQuotient + half][fourthsQuotient];
	magicSquare[fourthsQuotient + half][fourthsQuotient] = stored;
	for (let i = order; i > order - fourthsQuotient + 1; i--) {
		stored = 0;
		tmpRow = 0;
		while (tmpRow < half) {
			stored = magicSquare[tmpRow][i - 1];
			magicSquare[tmpRow][i - 1] = magicSquare[tmpRow + half][i - 1];
			magicSquare[tmpRow + half][i - 1] = stored;
			tmpRow++;
		}
	}
	return magicSquare;
}

/**
 * Initailizes a dummy magic square with the given order
 */
function initializeMagicSquare(order: number): number[][] {
	return new Array(order).fill(0).map(() => new Array(order).fill(0));
}

/**
 * Error Message
 */
function getErrMessage(): string {
	return "Please give an order which is a number between 3 and 110.";
}

/**
 * This gives the magic sum of the magic square.
 * @return int magicSum
 */
function getMagicSum(order: number): number {
	return (order * (order * order + 1)) / 2;
}

/**
 * This method characterizes the magic square.
 * @param int order of the magic square
 */
function discriminateIt(order: number): number {
	/**
	 * The type of the magic square
	 */
	let kind: number = 1;
	let residue = order % 4;

	switch (residue) {
		case 0:
			kind = 4;
			break;
		case 2:
			kind = 2;
			break;
	}
	return kind;
}

/**
 * This calls the appropriate methods to generate the magic square.
 * @return magic square as an integer array
 */
function generateMagicSquare(order: number): number[][] {
	let magicSquare: number[][] = [];
	let kind = discriminateIt(order);
	switch (kind) {
		case 1:
			return createOddMagicSquare(order);
		case 4:
			return createDoublyEvenMagicSquare(order);
		case 2:
			return createSinglyEvenMagicSquare(order);
	}
	return magicSquare;
}

/**
 * This prints the magic square.
 */
function printMagicSquare(magicSquare: number[][]) {
	for (let row = 0; row < magicSquare.length; row++) {
		console.log(...magicSquare[row]);
	}
}

/**
 * This prints the magic sum.
 */
function printMagicSum(magicSum: number) {
	console.log("The magic sum is:", magicSum);
}

/**
 * This writes the magic square to a file.
 */
async function writeMagicSquare(magicSquare: number[][]) {
	const fileName = "./magicsquare_" + magicSquare.length + ".csv";
	try {
		await Deno.remove(fileName);
	} catch (err) {}
	const encoder = new TextEncoder();
	const output = await Deno.open(fileName, {
		create: true,
		append: true,
	});
	const outputWriter = output.writable.getWriter();
	await outputWriter.ready;
	for (let row = 0; row < magicSquare.length; row++) {
		await outputWriter.write(encoder.encode(magicSquare[row].toString()));
		await outputWriter.write(encoder.encode(EOL));
	}
	await outputWriter.close();
}

// Main function
function main() {
	console.log(license());
	console.log();
	let bypass: boolean = false;
	let orderStr = prompt("Enter a number between 3 and 110: ");
	orderStr = orderStr === null ? "" : orderStr;
	let order = parseInt(orderStr);
	if (isNaN(order) || order < 3 || order > 110) {
		console.error(getErrMessage());
		bypass = true;
	}
	if (!bypass) {
		let save = prompt("Do you want to save the magic square? (y/N): ");
		console.log();
		let magicSquare = generateMagicSquare(order);
		if (magicSquare.length > 0) {
			printMagicSquare(magicSquare);
			console.log();
			printMagicSum(getMagicSum(order));
			if (save === "y" || save === "Y") {
				writeMagicSquare(magicSquare);
				console.log("The magicsquare has been written to disk");
			}
		}
	}
	prompt("\nHit 'Enter' to continue...");
}

if (import.meta.main) main();
