# MagicSquare-Deno-TypeScript

A magic square implementation in TypeScript for Deno.

## Getting started

deno run --allow-write magicsquare.ts


## License
Licensed under LGPL v3.
